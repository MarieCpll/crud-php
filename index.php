<?php

//connection BDD colyseum
$pdo= new PDO('mysql:host=localhost;dbname=colyseum','root','root');

// afficher tous les clients
echo "<h2>Voici tous nos clients :</h2>";

foreach ($pdo->query('SELECT * from clients') as $row){
	echo $row[firstName]." ".$row[lastName]."</br>";
}

// afficher tous les types de spectacles possibles
echo "<h2>Voici les types de shows possibles :</h2>";

foreach ($pdo->query('SELECT * from showTypes') as $row) {
	echo $row[type]."</br>";
}

// afficher les 20 premiers clients
echo "<h2>Voici nos 20 premiers clients de l'année :</h2>";

foreach ($pdo->query('SELECT * from clients LIMIT 20') as $row) {
	echo $row[firstName]." ".$row[lastName]."</br>";
}

// n'afficher que les clients possédants une carte de fidélité
echo "<h2>Voici nos clients les plus fidèles :</h2>";

foreach ($pdo->query('SELECT * from clients WHERE card= 1') as $row) {
	echo $row[firstName]." ".$row[lastName]."</br>";
}

?>